// alert('Hello, B176!);

/* 
    Objects
        - is a data type that is used to represent real world objects
        - it is also a collection of related data and/or functionalities

    Syntax:
        let objectName ={
            keyName: valueA,
            keyNameB: valueB
        }
*/

// Create an object
let cellphone = {
    name: "Nokia 3210",
    manufactureDate: 1999,
}

console.log(cellphone)
console.log(typeof cellphone)

// Creating objects using constructor function
/* 
    Constructors are like a blueprint or templates to create our objects form

    Syntax:
        function ObjectName(keyA, keyB) {
            this.keyA = keyA;
            this.keyB = keyB
        }

*/

function Laptop(name, manufactureDate, price) {
    this.name = name;
    this.manufactureDate = manufactureDate
    this.price = price
}

let laptop = new Laptop("Dell", 2008)
console.log(laptop)

let laptop2 = new Laptop("Acer", 2020)
console.log(laptop2)

let laptop3 = {
    name: "Asus",
    manufactureDate: 2020,
    price: 2000
}

let laptop4 = new Laptop("Asus", 2020)

// Old function
// function printName(name) {
//     console.log('My name is ${name}')
// }
// printName("Dexter")
// Create empty object
let computer = {}
let myComputer = new Object();
console.log(computer)
console.log(myComputer)

// Accessing Object Properties
// Using the dot notation
// Objectname.propertyName
// laptop = {name: 'Dell', manufactureDate: 2008}
console.log(laptop.name) // Dell
console.log(laptop.manufactureDate) // 2008

// Using the square bracket
console.log(laptop["name"])

// Accessing Array Objects
let array = [laptop, laptop2]

console.log(array[0]['name'])
console.log(array[1].name)

// Initializing/Adding/Reassigning object properties
let car = {}
console.log(car)

car.name = "Honda Civic";
console.log(car)

// laptop.price = 20000
// console.log(laptop)

car['manufacture date'] = 2019
console.log(car)

delete car['manufacture date']
console.log(car)

laptop = {
    name: "Toshiba",
    manufactureDate: 2010
}
console.log(laptop)

//car = {}

car.name = "Ferrari"
console.log(car)

let user = {
    name: "Dexter",
    email: ['dex@mail.com', 'dex1@mail.com']
}

console.log(user.length)
console.log(user.email[0].length)

// Object Methods
/*  
    - A method is a function which is a property of an object
    - They are also function and one of the key differences they have is that they are functions related to a specific object

*/

let person = {
    name: "Casi",
    talk: function () {
        console.log("Hello my name is " + this.name)
    }
}

console.log(person)
person.talk()

// Mini Activity

/* 
    Create a function named walk. "Casi walked 25 steps forward"

*/

// let action = {
//     name: "Casi",
//     walk: function () {
//         console.log(this.name + " walked 25 steps forward")
//     }
// }

// action.walk()

// Solution #1

person.walk = function () {
    console.log(this.name + " walked 25 steps forward");
}

person.walk()

// Real World Application of Objects
/* 

    Scenario: 
        1. We would like to create a game that would have several pokemon interact with each other.
        2. Every pokemon should have the same set of stats, properties and functions.

*/

// let myPokemon = {
//     name: "Pikachu",
//     level: 3,
//     health: 100,
//     attack: 50,
//     tackle: function () {
//         console.log("This pokemon tackled targetPokemon")
//         console.log("target health is now reduced to targetPokemonHealth")
//     },
//     faint: function () {
//         console.log("Pokemon fainted")
//     }
// }
// console.log(myPokemon)

function Pokemon(name, level) {
    // properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    // method
    this.tackle = function (target) {
        console.log(this.name + " tackled " + target.name + ". " + target.name + "'s health decreased by " + this.attack + ".")
        console.log(target.name + "'s health is now " + (target.health - this.attack) + ".")
        target.health = (target.health - this.attack)
        if (target.health <= 5) {
            target.faint()
        }

    };
    this.faint = function () {
        console.warn(this.name + " fainted.")
    }
}


let dragonite = new Pokemon("Dragonite", 66)
let blastoise = new Pokemon("Blastoise", 59)

console.log(dragonite)
console.log(blastoise)

// Activity:

// 1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
//     - Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
//         (target.health - this.attack)

// 2.) If health is below 5, invoke faint function.

